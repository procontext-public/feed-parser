<?php

namespace Procontext\FeedParser;

use Procontext\FeedParser\Exception\FeedFormatNotSupportedException;
use Procontext\FeedParser\Exception\FeedNotAvailableException;
use Symfony\Component\Console\Command\Command as BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use SimpleXMLElement;

class Command extends BaseCommand
{
    protected function getFeedData(string $feedPath, string $feedFormat): array
    {
        switch ($feedFormat) {
            case 'csv':
                $feedData = $this->readFromCsv($feedPath);
                break;
            case 'json':
                $feedData = $this->readFromJson($feedPath);
                break;
            case 'xml':
                $feedData = $this->readFromXml($feedPath);
                break;
            default:
                throw new FeedFormatNotSupportedException();
        }

        return $feedData;
    }

    private function readFromXml(string $feedPath): array
    {
        $xmlString = $this->getFeedContent($feedPath);
        $xml = simplexml_load_string($xmlString);

        return json_decode(json_encode($xml, JSON_UNESCAPED_UNICODE), true);
    }

    private function readFromCsv(string $feedPath): array
    {
        $csvString = $this->getFeedContent($feedPath);

        $csv = array_map(
            function ($elem) {
                return str_getcsv($elem);
            },
            explode("\r\n", $csvString)
        );

        if (count(end($csv)) !== count($csv[0])) {
            array_pop($csv);
        }

        array_walk(
            $csv,
            function (&$a) use ($csv) {
                $a = array_combine($csv[0], $a);
            }
        );
        array_shift($csv);

        return $csv;
    }

    private function readFromJson(string $feedPath): array
    {
        $jsonString = $this->getFeedContent($feedPath);

        return json_decode($jsonString, true);
    }

    private function getFeedContent(string $feedPath): string
    {
        if (!$content = file_get_contents($feedPath)) {
            throw new FeedNotAvailableException();
        }

        return $content;
    }

    protected function saveJson(string $filePath, array $items)
    {
        return file_put_contents($filePath, json_encode($items, JSON_UNESCAPED_UNICODE));
    }
}