<?php

namespace Procontext\FeedParser\Exception;

use Throwable;

class FeedNotAvailableException extends \Exception
{
    public function __construct($message = "Фид не доступен или пуст", $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}