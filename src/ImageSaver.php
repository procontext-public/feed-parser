<?php

namespace Procontext\FeedParser;

use \Exception;
use Imagick;
use Intervention\Image\Constraint;
use Intervention\Image\ImageManager;

class ImageSaver
{
    private $imageManager;
    private $urlPath;
    private $savePath;

    public function __construct(string $urlPath, string $savePath)
    {
        $this->imageManager = new ImageManager();
        $this->urlPath = $urlPath;
        $this->savePath = $savePath;
        if(!file_exists($this->savePath)) {
            mkdir($this->savePath, 0777, true);
        }
    }

    public function save(string $url, string $name,  ?int $width = null, ?int $height = null, string $ext = 'png'): ?string
    {
        try {
            $image = $this->imageManager->make($url);
        } catch (Exception $exception) {
            return null;
        }

        if($width || $height)
            $image->resize($width, $height, function (Constraint $constraint) {
                $constraint->aspectRatio();
            });

        $image->save($this->savePath . '/' . $name . '.' . $ext);
        return $this->urlPath . $name;
    }

    public function getOrientation($url)
    {
        try {
            $img = new Imagick($url);
            $orientation = $img->getImageOrientation();

            switch ($orientation) {
                case  imagick::ORIENTATION_TOPLEFT:         // 1
                    return imagick::ORIENTATION_TOPLEFT;
                case imagick::ORIENTATION_BOTTOMRIGHT:      // 3
                    return imagick::ORIENTATION_BOTTOMRIGHT;
                case imagick::ORIENTATION_RIGHTTOP:         //6
                    return imagick::ORIENTATION_RIGHTTOP;
                case imagick::ORIENTATION_LEFTBOTTOM:       // 8
                    return imagick::ORIENTATION_LEFTBOTTOM;
                default:
                    return imagick::ORIENTATION_UNDEFINED;
            }
        } catch (Exception $exception) {
            return null;
        }
    }
}